package com.lehuy.demo.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.lehuy.demo.entity.CatalogItem;
import com.lehuy.demo.entity.Movie;
import com.lehuy.demo.entity.UserRating;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogController {

	@Autowired
	private RestTemplate resTemplate;
	
//	@Autowired
//	private WebClient.Builder webClientBuilder;
	
	@GetMapping("/{userId}")
	public List<CatalogItem> getCatalog(@PathVariable("userId") String userId) {
		
		UserRating ratings = resTemplate.getForObject("http://rating-data-service/ratingdata/users/" + userId, UserRating.class);
		
		return ratings.getUserRating().stream().map(rating -> {
			Movie movie = resTemplate.getForObject("http://movie-info-service/movies/" + rating.getMovieId(), Movie.class);
//			Movie movie = webClientBuilder.build()
//				.get()
//				.uri("http://localhost:8082/movies/" + rating.getMovieId())
//				.retrieve().bodyToMono(Movie.class)
//				.block();
			
			return new CatalogItem(movie.getName(), "Test", rating.getRating());
		}).collect(Collectors.toList());
		
		//For each movieID, call movie service and get detail
		
		//put all together

	}
}
